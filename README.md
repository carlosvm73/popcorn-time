# Install Popcorn Time in Ubuntu
## Requirements
Privileged access to your Ubuntu System as root or via (sudo) command is required.
## Instructions
### Download Popcorn Time binaries
In this step we will download and extract Popcorn Time binaries into */opt/popcorn-time* directory:
```
$ sudo mkdir /opt/popcorn-time
FOR 32-BIT SYSTEM EXECUTE:
$ sudo wget -qO- https://get.popcorntime.sh/build/Popcorn-Time-0.3.10-Linux-32.tar.xz | sudo tar Jx -C /opt/popcorn-time
FOR 64-BIT SYSTEM EXECUTE:
$ sudo wget -qO- https://get.popcorntime.sh/build/Popcorn-Time-0.3.10-Linux-64.tar.xz | sudo tar Jx -C /opt/popcorn-time
```
### Create executable link
Now, that Popcorn Time binaries are in place, we will create a symbolic link of */opt/popcorn-time/Popcorn-Time* binary to */usr/bin/popcorn-time*. This will allow non-privileged users to start Popcorn Time via terminal without any additional configuration:
```
$  sudo ln -sf /opt/popcorn-time/Popcorn-Time /usr/bin/popcorn-time
```
### Create Desktop Launcher
Use your favorite text editor eg. nano and create launcher shortcut. The below command uses nano editor to create new */usr/share/applications/popcorntime.desktop* desktop entry: 
```
$ sudo vi /usr/share/applications/popcorntime.desktop
```
Insert the following content into the above created file:
```
[Desktop Entry]
Version = 1.0
Type = Application
Terminal = false
Name = Popcorn Time
Exec = /usr/bin/popcorn-time
Icon = /opt/popcorn-time/popcorntime.png
Categories = Application;
```
Lastly, download Popcorn Time's icon:
```
$ sudo wget -q -O /opt/popcorn-time/popcorntime.png https://upload.wikimedia.org/wikipedia/commons/6/6c/Popcorn_Time_logo.png
```
